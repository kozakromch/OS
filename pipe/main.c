#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int main () {

	printf("%s\n", "START");

	int fd[2];
	pid_t pid;
	
	if (pipe(fd) == 0) {
		
		char buffer[BUFSIZ];
		sprintf(buffer, "%d", fd[1]);
		FILE *transfer_fd = fopen("transfer", "w");
		
		if (transfer_fd == NULL) {
			printf("%s\n", "FILE problem");
			return -1;
		}
		
		fprintf(transfer_fd, "%s", buffer);
		if (fclose(transfer_fd) == -1) {
			printf("%s\n", "close transfer problem");
			return -1;
		}
		pid = fork();
		if (pid < 0) {
			printf("%s\n", "fork error");
			return -1;

		} if (pid == 0) {
			printf("%s\n", "PARENT START");

			if (close(fd[1]) == -1 ) {
				printf("%s\n", "close pipe-write-end problem");
				return -1;
			}

			char buf;
			while (read(fd[0], &buf, 1) > 0) {
		        	write(STDOUT_FILENO, &buf, 1);
			}
			printf("\n");
			if (close(fd[0]) == -1) {
				printf("%s\n", "parent: close pipe-read-end problem");
				return -1;
			}

			if (remove("transfer") == -1) {
				printf("\n%s\n", "parent: file deleting problem");			
			}

			printf("%s\n", "PARENT END");
		}
		if (pid > 0) {
			printf("%s\n", "CHILD START");
			if (close (fd[0]) == -1) {
				printf("%s\n", "child: close pipe-read-end problem");
				return -1;
			}

			execl("ch", "ch", NULL);
			printf("%s\n", "execl problem");
			return -1;
		}	 	
	} else {
		printf("%s\n", "pipe error");
		return -1;

	}

	printf("%s\n", "END");
	
	return 0;
}
	
		
