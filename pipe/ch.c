#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int main () {
	FILE *transfer_fd = fopen("transfer", "r");
		
	if (transfer_fd == NULL) {
		printf("%s\n", "execl: FILE problem");
		return -1;
	}
	
	char buf[BUFSIZ];	
	fgets(buf, BUFSIZ, transfer_fd);
	
	int fd =  atoi(buf);
	if (fd <= 0) {
		printf("%s\n", "execl: wrong pipe_fd");
		return -1;
	} 
	
	const char text[] = "HELLO DADDY";
	write(fd, text, strlen(text));
	if (close (fd) == -1) {
		printf("%s\n", "execl: close pipe-write-end problem");
		return -1;	
	}
	if (fclose (transfer_fd) == -1) {
		printf("%s\n", "execl: close transfer problem");
		return -1;	
	}
		
	return 0;
}
