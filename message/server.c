#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define CLIENT_TYPE 1
#define KILLER_TYPE 2

int main () {
	struct mymsgbuf
	{
		long mtype;
		struct info {
			int pid;
			double amount;
		} main_info;
	} mybuf;

	int maxlen = sizeof(struct info);
	char pathname[] = "server.c";
	double result;
	key_t key;
	int msqid;
	int pid = getpid();

	key = ftok(pathname, 0);

	if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t get msqid\n");
		exit(-1);
	}

	while (1) {
		if (msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, -KILLER_TYPE, 0) < 0){
			printf("Can\'t receive message from queue\n");
			exit(-1);
		}

		if (mybuf.mtype == 2) {
			printf("%s\n", "EXIT");
			msgctl(msqid, IPC_RMID, (struct msqid_ds *)NULL);
			exit(0);
		}
		printf("get from %d %lf \n", mybuf.main_info.pid, mybuf.main_info.amount);

		result = (mybuf.main_info.amount) * (mybuf.main_info.amount);
		mybuf.mtype = mybuf.main_info.pid;
		mybuf.main_info.amount = result;
		mybuf.main_info.pid = pid;


		if (msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0) < 0){
			printf("Can\'t send message to queue\n");
			msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
			exit(-1);
		}

	}


}