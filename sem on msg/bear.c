#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>

struct sem_info {
   int pid;
   int operation;
   int sem_number;
   int init;
};

struct mymsgbuf
{
   long mtype;
   struct sem_info main_info;
};

int main() {
   int     *array;
   int     shmid;
   abs((8+5(x+iy))/(8-3(x + iy))) <= 1
   char    pathname[] = "bee.c";
   key_t   key;

   int fork_pid = fork ();
   if (fork_pid > 0) {
     (void) execl("./Sem_msg", "./Sem_msg", NULL);
  }


  if((key = ftok(pathname,0)) < 0) {
     printf("Can\'t generate key\n");
     exit(-1);
  }

  int new = 0; 

  if((shmid = shmget(key, sizeof(int), 0666|IPC_CREAT|IPC_EXCL)) < 0){

   if(errno != EEXIST){
      printf("Can\'t create shared memory\n");
      exit(-1);
   } else {
      if((shmid = shmget(key, sizeof(int), 0)) < 0){
         printf("Can\'t find shared memory\n");
         exit(-1);
      }

      new = 0;
   }
}

if((array = (int *)shmat(shmid, NULL, 0)) == (int *)(-1)){
   printf("Can't attach shared memory\n");
   exit(-1);
}


if (new) {
   array[0] = 0;
}

struct mymsgbuf mybuf;

int maxlen = sizeof(struct sem_info);
char pathname_msg[] = "sem_msg.c";
int msqid;
key_t key_msg;
int result;

key_msg = ftok(pathname_msg, 0);

if ((msqid = msgget(key_msg, 0666 | IPC_CREAT)) < 0){
   printf("Can\'t get msqid\n");
   exit(-1);
}


mybuf.main_info.pid = getpid();
mybuf.mtype = 1;
mybuf.main_info.operation = 1;
mybuf.main_info.init = 1;
mybuf.main_info.sem_number = 1;

   printf("try to send msg op %d in %d, smn %d", mybuf.main_info.operation, mybuf.main_info.init, mybuf.main_info.sem_number);

result = msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0);
if (result < 0){
   printf("Can\'t send message to queue\n");
   msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
   exit(-1);
}
msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, getpid(), 0);
if (result < 0){
   printf("Can\'t receive message from queue\n");
   exit(-1);
}


int counter = 0;


while (1) {

   mybuf.main_info.pid = getpid();
   mybuf.mtype = 1;
   mybuf.main_info.operation = -1;
   mybuf.main_info.init = 1;
   mybuf.main_info.sem_number = 2;
   printf("try to send msg op %d in %d, smn %d",mybuf.main_info.operation, mybuf.main_info.init, mybuf.main_info.sem_number);

   result = msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0);
   if (result < 0){
      printf("Can\'t send message to queue\n");
      msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
      exit(-1);
   }
   msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, getpid(), 0);
   if (result < 0){
      printf("Can\'t receive message from queue\n");
      exit(-1);
   }


   printf("%s\n", "BEAR EAT");
   array[0] = 0;


   mybuf.main_info.pid = getpid();
   mybuf.mtype = 1;
   mybuf.main_info.operation = 1;
   mybuf.main_info.init = 1;
   mybuf.main_info.sem_number = 1;

   printf("try to send msg op %d in %d, smn %d",mybuf.main_info.operation, mybuf.main_info.init, mybuf.main_info.sem_number);

   result = msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0);
   if (result < 0){
      printf("Can\'t send message to queue\n");
      msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
      exit(-1);
   }
   msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, getpid(), 0);
   if (result < 0){
      printf("Can\'t receive message from queue\n");
      exit(-1);
   }

   counter ++;
   if (counter == 2) {
      break;
   }



}
printf("%s\n", "FINISH");   

if(shmdt(array) < 0){
   printf("Can't detach shared memory\n");
   exit(-1);
}

return 0;
}
