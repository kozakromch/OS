#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main () {

	char    pathname[] = "a.c";
	key_t 	key;
	int 	shmid;
	char* 	array;

	if((key = ftok(pathname, 0)) < 0){
		printf("Can\'t generate key\n");
		exit(-1);
	}
	if((shmid = shmget(key, 5000*sizeof(char), 0666)) < 0){
		printf("Can\'t find shared memory\n");
		exit(-1);
	}

	if((array = (char *)shmat(shmid, NULL, 0)) == (char *)(-1)){
		printf("Can't attach shared memory\n");
		exit(-1);
	}

	printf("%s\n", array);
	
}