#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct sembuf init(int num, int op, int flg) {
	struct sembuf buf;
	buf.sem_num = num;
	buf.sem_op  = op;
	buf.sem_flg = flg;
	return buf;
}

int main () {

	int   semid;
	char pathname[] = "board.c";
	key_t key = ftok(pathname, 0);
	struct sembuf mybuf;
	int N = 3;

	if((semid = semget(key, 3, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t create semaphore set\n");
		exit(-1);
	}
	int i = 0;
	for (i = 0; i != 3; i++) {
		printf("%s\n", "loading board");
		mybuf = init(1, N, 0);
		if(semop(semid, &mybuf, 1) < 0) {
			printf("sem board 1 \n");
			return -1;
		}
		printf("%s\n", "block here 1 board");
		mybuf = init(0, -N, 0);
		if(semop(semid, &mybuf, 1) < 0) {
			printf("sem board 2 \n");
			return -1;
		}
		printf("%s\n", "walking");
		mybuf = init(2, N, 0);
		if(semop(semid, &mybuf, 1) < 0) {
			printf("sem board 3 \n");
			return -1;
		}
		printf("%s\n", "block here 2 board");
		mybuf = init(0, -N, 0);
		if(semop(semid, &mybuf, 1) < 0) {
			printf("sem board 4 \n");
			return -1;
		}
		printf("%s\n", "unloading");

	}
	return 0;

}