#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define CLIENT_TYPE 1

int main () {
	struct mymsgbuf
	{
		long mtype;
		struct info {
			int pid;
			double amount;
		} main_info;
	} mybuf;

	int maxlen = sizeof(struct info);
	char pathname[] = "server.c";
	int msqid;
	key_t key;

	key = ftok(pathname, 0);

	if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t get msqid\n");
		exit(-1);
	}

	
	mybuf.main_info.pid = getpid();
	double answer;
	scanf("%lf", &answer);
	printf("%lf\n", answer);
	printf("%d\n", getpid());

	mybuf.main_info.amount = answer;
	printf("%lf\n", mybuf.main_info.amount);

	mybuf.mtype = 1;

	if (msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0) < 0){
		printf("Can\'t send message to queue\n");
		msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
		exit(-1);
	}

	if (msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, getpid(), 0) < 0){
		printf("Can\'t receive message from queue\n");
		exit(-1);
	}
	printf("result from server %lf ", mybuf.main_info.amount);
}