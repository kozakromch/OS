#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>

int num = 0;
pid_t pid;
int i = sizeof(int) * 8;
int buf = 1;

void my_handler(int nsig){
	i --;
	switch (nsig)
	{
		case SIGUSR1:
		num += buf; 
		kill(pid, SIGUSR1); 
		break;
		case SIGUSR2:
		kill(pid, SIGUSR2);
		break;
	}
	buf *= 2;
	printf("%d\n", num);
	printf("Receive signal %d\n", nsig);
	if (i == 0) {
		exit(0);
	}
}

int main(void){

	signal(SIGUSR1, my_handler);
	signal(SIGUSR2, my_handler);
	printf("%d", getpid());
	scanf ("%d", &pid);

	kill(pid, SIGUSR1);


	while(1);
	return 0;

}