#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

struct sembuf init(int num, int op, int flg) {
	struct sembuf buf;
	buf.sem_num = num;
	buf.sem_op  = op;
	buf.sem_flg = flg;
	return buf;
}

int main () {

	pid_t pid = 0, wpid;
	int amount = 9;
	int   semid;
	char pathname[] = "board.c";
	key_t key = ftok(pathname, 0);
	struct sembuf mybuf[2];
	int status = 0;

	if((semid = semget(key, 3, 0666 )) < 0){
		printf("Can\'t create semaphore set\n");
		exit(-1);
	}

	for(;amount > 0; amount --) {
		pid = fork();
		if (pid == 0) {//пассажиры
			printf("%s %d\n", "Im pass", pid);
			mybuf[0] = init(0, 1, 0);
			mybuf[1] = init(1, -1, 0);
			if(semop(semid, mybuf, 2) < 0) {
				printf("sem pass 1 \n");
				return -1;
			}	

			printf("%s %d\n", "pass walking", pid);

			mybuf[0] = init(2, -1, 0);
			if(semop(semid, mybuf, 1) < 0) {
				printf("sem pass 1 \n");
				return -1;
			}	
			printf("%s %d\n", "pass exit walking", pid);
			mybuf[0] = init(0, 1, 0);
			if(semop(semid, mybuf, 1) < 0) {
				printf("sem pass 1 \n");
				return -1;

			}
			printf("%s %d\n", "exit", pid);
			return 0;

		}
		if (pid < 0) {
			printf("%s\n", "fork error");
			return -1;
		}

	}
	while ((wpid=waitpid(-1, &status, 0)) != -1) {
		printf("Process %d finish\n", wpid);

	}
	printf("%s\n", "FINISH");
	return 0;
}