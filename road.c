#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

struct sembuf init(int num, int op, int flg) {
	struct sembuf buf;
	buf.sem_num = num;
	buf.sem_op  = op;
	buf.sem_flg = flg;
	return buf;
}

void general(int semid) {
	struct sembuf mybuf[3];
	pid_t pid = getpid();

	printf("%s %d\n", "general", pid);
	mybuf[0] = init(0, -1, 0);
	mybuf[1] = init(2, 1, 0);
	if(semop(semid, mybuf, 2) < 0) {
		printf("sem general 1 \n");
		exit (-1);
	}

	printf("%s %d\n", "second zone", pid);

	mybuf[0] = init(0, 1, 0);
	mybuf[1] = init(1, -1, 0);
	if(semop(semid, mybuf, 2) < 0) {
		printf("sem general 2 \n");
		exit (-1);
	}

	printf("%s %d\n", "first zone from general", pid);

	mybuf[0] = init(1, 1, 0);
	mybuf[1] = init(2, -1, 0);
	if(semop(semid, mybuf, 2) < 0) {
		printf("sem general 3 \n");
		exit (-1);
	}

	printf("%s %d\n", "success general", pid);
	exit (0);

}

void secondary (int semid) {
	struct sembuf mybuf[3];
	pid_t pid = getpid();	

	printf("%s %d\n", "secondary", pid);
	
	mybuf[0] = init(1, -1, 0);
	mybuf[1] = init(2, 0, 0);
	if(semop(semid, mybuf, 2) < 0) {
		printf("sem secondary 1 \n");
		exit (-1);
	}

	printf("%s %d\n", "first zone from secondary", pid);
	
	mybuf[0] = init(1, 1, 0);
	if(semop(semid, mybuf, 1) < 0) {
		printf("sem secondary 2 \n");
		exit (-1);
	}

	printf("%s %d\n", "success general", pid);

}

int main () {

	printf("%s\n", "START");
	int   semid;
	char pathname[] = "road.c";
	key_t key = ftok(pathname, 0);
	int N = 100;
	pid_t pid;
	struct sembuf mybuf[3];

	if((semid = semget(key, 3, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t create semaphore set\n");
		exit(-1);
	}
	
	mybuf[0] = init(0, 1, 0);
	mybuf[1] = init(1, 1, 0);
	if(semop(semid, mybuf, 2) < 0) {
		printf("sem main 1 \n");
		return -1;
	}//S1(1), S2(1), S3(0)

	int i = 0;
	for (i = 0; i != N; i++) {//генерим машины rand-ом определяем главные и второстепенные 
		pid = fork();
		if (pid == 0) {
			if (i % 2){//главная
				
				general(semid);
				fflush(0);
				return 0;

			} else {//второстепенная
				
				secondary(semid);
				fflush(0);
				return 0;

			}
		}
		if (pid < 0) {
			printf("%s\n", "fork error");
			return -1;
		}
	}

	return 0;
}