#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

struct sembuf init(int num, int op, int flg) {
	struct sembuf buf;
	buf.sem_num = num;
  buf.sem_op  = op;
  buf.sem_flg = flg;
  return buf;
}

int main(int argc, char *argv[], char *envp[])
{

  int   semid;
  char pathname[]="sem.c";
  key_t key;
  struct sembuf mybuf;
  int fd[0];
  int N = 3;
  char string[12];

  if (pipe(fd) < 0) {
    printf("%s\n", "pipe problem");
    return -1;
  }

  key = ftok(pathname, 0);

  if((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0){
    printf("Can\'t create semaphore set\n");
    exit(-1);
  }

  int pid = fork();

  if (pid == 0) {// parent

    int i = 0;

    for (i = 0; i <= N; i++) {
      mybuf = init(0, -3, 0);
      if(semop(semid, &mybuf, 1) < 0) {
       printf("cond parent 3 \n");
       return -1;
     }

     if (read(fd[0], string, 12) < 0) {
       printf("%s\n", "read parent problem");
     }
     printf ("%s\n", string);

     if (write(fd[1], "hello_child", 12) < 0) {
       printf("%s\n", "write parent problem");
     }

     mybuf = init(0, 5, 0);
     if(semop(semid, &mybuf, 1) < 0) {
       printf("cond parent 3 \n");
       return -1;
     }

     mybuf = init(0, 0, 0);
     if(semop(semid, &mybuf, 1) < 0) {
       printf("cond parent 3 \n");
       return -1;
     }
   }

 }
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

    if (pid > 0) {//child      
      int i = 0;

      for (i = 0; i <= N; i++) {
        mybuf = init(0, 2, 0);
        if(semop(semid, &mybuf, 1) < 0) {
         printf("cond child 1 \n");
         return -1;
       }

       if (write(fd[1], "helloparent", 12) < 0) {
         printf("%s\n", "write child problem");
       }

       mybuf = init(0, 1, 0);
       if(semop(semid, &mybuf, 1) < 0) {
         printf("cond child 2 \n");
         return -1;
       }

       mybuf = init(0, -4, 0);
       if(semop(semid, &mybuf, 1) < 0) {
         printf("cond child 3 \n");
         return -1;
       }

       if (read (fd[0], string, 12));
       printf("%s\n", string);
       mybuf = init(0, -1, 0);
       if(semop(semid, &mybuf, 1) < 0) {
         printf("cond child 3 \n");
         return -1;
       }	

     }
   }

   if (pid < 0) {
     printf("%s\n", "fork problem");
     return -1;
   }
}