#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define FIRST 10
#define SECOND 20
int main(void)
{

	int msqid;
	char pathname[] = "reseive_get_1.c";
	key_t  key;
	int i,len;

	struct mymsgbuf
	{
		long mtype;
		struct info {
			char text [90];
			int number;
		} main_info;
	} mybuf;

	int maxlen = sizeof(struct info);

	key = ftok(pathname, 0);

	if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t get msqid\n");
		exit(-1);
	}

	for (i = 1; i <= 5; i++) {
		mybuf.main_info.number = i;
		printf("%s\n", "LIVE");
		strcpy(mybuf.main_info.text, "Hello");
		len = sizeof(struct info);
		mybuf.mtype = FIRST;

		if (msgsnd(msqid, (struct msgbuf *) &mybuf, len, 0) < 0){
			printf("Can\'t send message to queue\n");
			msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
			exit(-1);
		}
		printf("%s\n", "STILL ALIVE");
	}
	printf("1\n");
	for (i = 1; i <= 5; i++) {
		printf("%s\n", "RESEIVE");
		if (( len = msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, SECOND, 0)) < 0){
			printf("Can\'t receive message from queue\n");
			exit(-1);
		}

		printf("%s %d", mybuf.main_info.text, mybuf.main_info.number);
	}
}