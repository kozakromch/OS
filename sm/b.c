#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main()
{
	char     *array;
	int     shmid;
	char    pathname[] = "a.c";
	key_t   key;
	char    name[] = "b.c";
	int		fd;
	char 	resstring[5000];
	int 	size;
	
	if((key = ftok(pathname, 0)) < 0){
		printf("Can\'t generate key\n");
		exit(-1);
	}

	if((shmid = shmget(key, 5000*sizeof(char), 0666 | IPC_CREAT)) < 0){
		printf("Can\'t find shared memory\n");
		exit(-1);
	}

	if((array = (char *)shmat(shmid, NULL, 0)) == (char *)(-1)){
		printf("Can't attach shared memory\n");
		exit(-1);
	}


	if((fd = open(name, O_RDONLY)) < 0){
		printf("Can\'t open file for reading\n");
		exit(-1);
	}

	size = read(fd, resstring, 5000);

	if(size < 0){
		printf("Can\'t read string from FIFO\n");
		exit(-1);
	}
	int i = 0;
	for (i = 0; i < 5000; i++) {
		array[i] = resstring[i];
	}
	printf("%s\n", array);
	printf("%s\n", "--------------------------------------------");
	printf("%s\n", resstring);

	if(shmdt(array) < 0){
		printf("Can't detach shared memory\n");
		exit(-1);
	}

	return 0;
}