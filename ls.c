#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

//for color print
#define RESET   "\033[0m"
#define RED     "\033[1;31m"
#define YELLOW  "\033[1;33m"
#define GREEN 	"\033[32m"
#define BLUE 	"\033[34m"
#define VIOLET 	"\033[35m"
#define WHITE   "\033[1;37m"
#define WAVE 	"\033[35m"


#define REG 	1
#define DIREC 	2
#define FIFO 	3
#define BLK 	4
#define CHR 	5
#define LNK 	6
#define SOCK 	7

struct program_option {
	int all;  // -a
	int size; // -s
	int l_time; // -t
	int uid; //-u user id
	int gid; // -g group id
	int link; // -l show where link file aimed
};

void usage() {
	printf("\n-h --help usage \n");
	printf("\n-a --all  list all files\n");
	printf("\n-s --size print size of file\n");
	printf("\n-t --time print time of last modification \n");
	printf("\n-u --uid print uid\n");
	printf("\n-g --gid print gid \n");
	printf("\n-l --link print linked file\n");
	printf("\nwhite - regular files\n");
	printf("\nred -dirrectory\n");
	printf("\nviolet - fifo\n");
	printf("\nblue - block\n");
	printf("\nyellow - chr file\n");
	printf("\ngreen - link\n");
	printf("\nwave - socket\n");

}

int get_type_of_file(mode_t m){
	int mode = 0;
	if (S_ISREG(m)) {
		mode = REG;
	} else if (S_ISDIR(m)) {
		mode = DIREC;
	} else if (S_ISFIFO(m)){
		mode = FIFO;
	}else if (S_ISBLK(m)) {
		mode = BLK;
	}else if (S_ISCHR(m)) {
		mode = CHR;
	}else if (S_ISLNK(m)) {
			//char buf_str[255]; 
			//readlink(new_filename, buf_str, 254);
			//printf("   %s\n", buf_str);
		mode = LNK;
	}else if (S_ISSOCK(m)) {
		mode = SOCK;
	} else {
		mode = 0;
	}
	return mode;
}

void list_info(struct program_option current_option, char* filename) {
	
	DIR* directory = opendir(filename);
	struct dirent* current_file; 
	char new_filename[255] = "";
	struct stat buf;

	while((current_file = readdir(directory)) != NULL) {
		if ((!current_option.all) && (current_file->d_name[0] == '.')) {
			continue;
		}

		new_filename[0] = '\0';
		//создаем имя файла которое будем парсить
		strcat(new_filename, filename);
		strcat(new_filename, "/");
		strcat(new_filename, current_file->d_name);

		if (lstat(new_filename, &buf) < 0 ) {
			printf("%s\n", "error"); 
		} // загребаем инфу о файле

		int mode = get_type_of_file(buf.st_mode);

		switch (mode) {
			case REG:
			printf("%s", WHITE);
			printf("%6s ", "reg");
			break;
			case DIREC:
			printf("%s", RED);
			printf("%6s ", "dir");
			break;
			case FIFO:
			printf("%s", VIOLET);
			printf("%6s ", "fifo");
			break;
			case BLK:
			printf("%s", BLUE);
			printf("%6s ", "block");
			break;
			case CHR:
			printf("%s", YELLOW);
			printf("%6s ", "chr");
			break;
			case LNK:
			printf("%s", GREEN);
			printf("%6s ", "link");
			break;
			case SOCK:
			printf("%s", WAVE);
			printf("%6s ", "socket");
			break;
			case 0:
			printf("%s", "Do not know");
			break;
		}

		printf(current_file->d_name);// печатаем имя файла
		printf("%s", RESET);// скидываем цвет
		//выполняем опции
		if (current_option.size) {
			printf(" size:%5d bytes ", buf.st_size);
		}
		if (current_option.l_time) {
			printf(" time:%5d ", buf.st_size);
		}
		if (current_option.uid) {
			printf(" uid:%4d ", buf.st_uid);
		}
		if (current_option.gid) {
			printf(" gid:%4d ", buf.st_gid);
		}
		if ((current_option.link) && (mode == LNK)) {
			char buf_str[255]; 
			readlink(new_filename, buf_str, 254);
			printf("   %s", buf_str);
		}
		printf("\n");
	}
}



int main (int argc, char* argv[]) {
	
	static struct option long_opt[] = {
		{"help", 0, 0, 'h'},
		{"all", 0, 0, 'a'},
		{"size", 0, 0, 's'},
		{"time", 0, 0, 't'},
		{"user", 0, 0, 'u'},
		{"group", 0, 0, 'g'},
		{"link", 0, 0, 'l'},
		{"filename", 1, 0, 'n'},
		{0, 0, 0, 0}
                  };//struct need for parsing getopt_long

                  struct program_option current_option;
                  current_option.all = 0;
                  current_option.size = 0;
                  current_option.l_time = 0;
                  current_option.uid = 0;
                  current_option.gid = 0;
                  current_option.link = 0;

	char filename [255] = ".";//дефолтно смотрим на текущую директорию

	int opt;
	while ((opt = getopt_long(argc, argv, "hastugln:", long_opt, NULL)) != -1) {
		switch (opt) {
			case 'h':
			usage();
			return 0;
			case 'a':
			current_option.all = 1;
			break;
			case 's':
			current_option.size = 1;
			break;
			case 't':
			current_option.l_time = 1;
			break;
			case 'u':
			current_option.uid = 1;
			break;
			case 'g':
			current_option.gid = 1;
			break;
			case 'l':
			current_option.link = 1;
			break;
			case 'n':
			strcpy(filename, optarg);
			break;	
		}
	}

	if(optind < argc) {
		strcpy(filename, argv[optind]);
	}

	printf(filename);
	printf("\n");

	list_info(current_option, filename);

	return 0;

}