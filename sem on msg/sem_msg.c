#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


struct sem_info {
	int pid;
	int operation;
	int sem_number;
	bool init;
};

struct mymsgbuf
{
	long mtype;
	struct sem_info main_info;
}

int main () {
	struct mymsgbuf mybuf;

	struct sem_info main_sem_info [100] ;// array contain the info about all semaphores
	int length_main = 0;

	int sem_cond [2][100];
	int length_cond = 0; 
	// number | condition
	// 2	  |   +4

	int maxlen = sizeof(struct sem_info);
	char pathname[] = "sem_msg.c";
	int msqid;
	key_t key;

	key = ftok(pathname, 0);

	if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t get msqid\n");
		exit(-1);
	}
	int i;
	bool exist_flag = 0;
	while (1) {

		if (msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, 1, 0) < 0) {
			printf("Can\'t receive message from queue\n");
			exit(-1);
		}
		//пришло сообщение с инициализацией. 
		if (mybuf.main_info.init == 1) {
			exist_flag = 0;
			for (i = 0; i < length_cond; i++) {
				if (sem_cond[1][i] == mybuf.main_info.sem_number) {
					exist_flag = 1;
					break;
				}
			} if (exist_flag) {
				sem_cond[1][length_cond] = mybuf.main_info.sem_number;
				sem_cond[2][length_cond] = 0;
				length_cond ++;
			}
		}
		//если отнимаем или ждем нуля добавляем в главный массив  будем обрабатывать
		if (mybuf.main_info.operation <= 0) { 
			main_sem_info[length] = mybuf.main_info;
			length_main ++;
		} else {
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////
		// если прибавляем меняем состояние в sem_cond, отсылаем разблокировку
			exist_flag = 0; //по дефолту считаем что семафора нет
			for (i = 0; i < length_cond; i++) {
				if (sem_cond[1][i] == mybuf.main_info.sem_number) {
					sem_cond[2][i] += mybuf.main_info.operation;
					exist_flag = 1;
					break;
				}
			}
			//если не существует семафора в списке. значит не был проинициализирован. 
			//Разблокируем процесс напишем на экран
			
			if (!exist_flag) {
				printf("%s\n", "No semaphore with number %d by process %d", \
					mybuf.main_info.sem_number, mybuf.main_info.pid);
			}

			//отправляем сообщение для разблокировки 

			mybuf.type = mybuf.main_info.pid;
			mybuf.main_info.pid = getpid();

			if (msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0) < 0){
				printf("Can\'t send message to queue\n");
				msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
				exit(-1);
			}
		}
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////
		//проходимся по всему массиву если есть возможность менять состояние меняем,
		// отправляем разблокировку, обнуляем j проходимся заново по всему массиву
		int j;
		int sem_number_buf;
		for (i = 0; i < length_cond; i++)
		{
			sem_number_buf = sem_cond[1][i];

			for (j = 0; j < length_main; j++) {
				if (sem_number_buf == main_sem_info[j].sem_number)
					if ((sem_cond[2][i] == 0) && (main_sem_info[j].operation == 0)\
							|| (sem_cond[2][i] > main_sem_info[j].operation)) {
						
						mybuf.type = main_sem_info[j].pid;
						mybuf.main_info.pid = getpid();

						if (msgsnd(msqid, (struct msgbuf *) &mybuf, maxlen, 0) < 0){
							printf("Can\'t send message to queue\n");
							msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
							exit(-1);
						}
						delete_sem_array_elem(main_sem_info, j);
						j = 0;
					}
				}

			}
		}
	}

	return 0;
}