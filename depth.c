#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main () {
	int fd;
	fd = open("0", O_CREAT | 0666);
	if (fd < 0 )
		printf("%s\n", "couldn't create");
	close(fd);
	int i = 0;
	char name_new[10];
	char name_old[10];
	while (1) {
		sprintf(name_old, "%d", i);
		i++;
		sprintf(name_new, "%d", i);

		if (symlink(name_old, name_new) < 0 ) {
			printf("%s %d\n", "couldn't create symlink in ", i);
			return 0;
		}

		fd = open(name_new, O_WRONLY);
		if (fd < 0) {
			printf("%s  %d\n", "FINISH", i);
			return 0;
		}
	}
	close (fd);
}

